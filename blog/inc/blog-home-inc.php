<?php
$Read->ExeRead(TB_USERS, "WHERE user_status = :st", "st=0");
$authors = $Read->getResult();
?>

<div class="bg-light blog-home blog-01">
	<div class="container">
		<div class="wrapper">
			<h2 class="text-center">Blog Home Padrão recentes</h2>
			<div class="grid-col-3">
				<?php
				$Read = new Read;
				$Read->ExeRead(TB_BLOG, "WHERE blog_status = :stats ORDER BY blog_id DESC LIMIT 0, 3", "stats=2");
				if ($Read->getResult()) :
					foreach ($Read->getResult() as $dados) :
						extract($dados); ?>
						<div class="blog-card">
							<div class="blog-card__image">
								<a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>">
									<img class="blog-card__cover" src="<?= RAIZ ?>/doutor/uploads/<?= $blog_cover ?>" alt="<?= $blog_title ?>" title="<?= $blog_title ?>">
								</a>
							</div>

							<div class="blog-card__info">
								<h3 class="blog-card__title"><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog_name; ?>" title="<?= $blog_title; ?>"><?= $blog_title ?></a></h3>
								<?php $newDate = explode("/", date("d/m/Y", strtotime($blog_date)));
								$blogDay = $newDate[0];
								$blogMonth = $newDate[1];
								$blogYear = $newDate[2];
								$blogFullDate = $blogDay . " de " . $blogMonthList[$blogMonth - 1] . " de " . $blogYear;
								?>
								<p class="blog-card__date"><?= $blogFullDate ?></p>

								<div class="blog-card__description">
									<?php if (BLOG_BREVEDESC && isset($blog_brevedescription)) :
										echo $blog_brevedescription;
									else : ?>
										<p class="blog-card__content-text"><?= Check::Words($blog_content, 25); ?></p>
									<?php endif; ?>
								</div>
							</div>
						</div>
				<? endforeach;
				endif; ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>