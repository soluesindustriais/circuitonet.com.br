<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <?php
    $lv = 1;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>
  </div>
  <div class="page-title">
    <div class="title col-md-12 col-sm-6 col-xs-12">
      <h3><i class="fa fa-money"></i> Lista de orçamentos</h3>
    </div>
    <div class="clearfix"></div>
    <br/>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <?php
          $ReadOrc = new Read;
          $ReadOrc->ExeRead(TB_ORCAMENTOS, "WHERE user_empresa = :emp ORDER BY id_orc DESC LIMIT 0, 150", "emp={$_SESSION['userlogin']['user_empresa']}");
          if (!$ReadOrc->getResult()):
            WSErro("Nenhum orçamento foi encontrado.", WS_ALERT, null, "MPI Technology");
          else:
            ?>
            <div class="x_content">
              <div class="x_title">
                <h2>Visualize, exporte, imprima e copie seus orçamentos</h2>
                <div class="clearfix"></div>
              </div>
              <br/>
              <div class="table-responsive">
                <table id="datatable-buttons" class="table table-striped dtr-inline dataTable jambo_table nowrap">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Ações</th>
                      <th>Data</th>
                      <th>Nome</th>
                      <th>E-mail</th>
                      <th>Telefone</th>
                      <!-- <th>Celular</th> -->
                      <!-- <th>Valor</th> -->
                      <!-- <th>Prioridade</th> -->
                      <?php if (!empty($orc_anexo)): ?>
                        <th>Anexo</th>
                      <?php endif ?>
                      <th>Cidade</th>
                      <th>Estado</th>
                      <!-- <th>Frase de pesquisa</th> -->
                      <!-- <th>Empresa</th> -->
                      <!-- <th>Segmento</th> -->
                      <!-- <th>Aplicação</th> -->
                      <th>Mensagem</th>
                      <!-- <th>Como nos conheceu?</th> -->
                      <th>Resumo do orçamento</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($ReadOrc->getResult() as $orcamentos):
                      extract($orcamentos);
                      ?>
                      <tr class="j_item" id="<?= $id_orc; ?>" scope="row">
                        <td><?= $id_orc; ?></td>
                        <td style="width: 70px;">
                          <div class="btn-group btn-group-xs">
                            <!--<button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=orcamentos/response&id=<?= $id_orc; ?>'"><i class="fa fa-mail-reply"></i></button>-->
                            <button type="button" class="btn btn-danger j_remove" rel="<?= $id_orc; ?>" action="RemoveOrcamento"><i class="fa fa-trash"></i></button>
                          </div>
                        </td>
                        <td><?= date("d/m/Y H:i", strtotime($orc_data)); ?></td>
                        <td><?= $orc_nome; ?></td>
                        <td><?= $orc_email; ?></td>
                        <td><?= $orc_telefone; ?></td>
                        <!-- <td><?= $orc_celular; ?></td> -->
                        <!-- <td><?= $orc_media; ?></td> -->
                        <!-- <td><?= $orc_previsao; ?></td> -->
                        <?php if (!empty($orc_anexo)): ?>
                          <td><a href="uploads/<?= $orc_anexo; ?>" title="Anexo de <?= $orc_nome; ?>" target="_blank" class="j_modalBox">Visualizar</a></td>
                        <?php endif ?>
                        <td><?= $orc_cidade; ?></td>
                        <td><?= $orc_uf; ?></td>
                        <!-- <td><?= $orc_frase; ?></td> -->
                        <!-- <td><?= $orc_empresa; ?></td> -->
                        <!-- <td><?= $orc_segmento; ?></td> -->
                        <!-- <td><?= $orc_aplicacao; ?></td> -->
                        <td><?= $orc_mensagem; ?></td>
                        <!-- <td><?= $orc_question; ?></td> -->
                        <td>
                          <table class="table table-striped jambo_table">
                            <?= $orc_cart; ?>
                          </table>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>