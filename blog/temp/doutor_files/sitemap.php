<?php


$last_run = fopen($fileName, "w+");
fwrite($last_run, date('Y-m-d'));
fclose($last_run);


$Read->ExeRead(TB_BLOG);

$date = new DateTime('now', new DateTimeZone('America/Sao_Paulo'));
$iso8601Date = $date->format('Y-m-d\TH:i:sP');

$resultsBlog = $Read->getResult();
$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https' : 'http';
$link = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$urlPage = $protocol . "://" . $link . "";

$countLink = 0;
$filename = 1;



$handle = fopen("sitemap-$filename.xml", 'w+');
fwrite($handle, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL);
fwrite($handle, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL);

foreach ($resultsBlog as $blog) :


  fwrite($handle, '  <url>' . PHP_EOL);
  fwrite($handle, '    <loc>' . $urlPage . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog['blog_name'] . '</loc>' . PHP_EOL);
  fwrite($handle, '    <lastmod>' . $iso8601Date . '</lastmod>' . PHP_EOL);
  fwrite($handle, '    <changefreq>weekly</changefreq>' . PHP_EOL);
  fwrite($handle, '    <priority>0.8</priority>' . PHP_EOL);
  fwrite($handle, '  </url>' . PHP_EOL);

  $countLink++;

  // Se atingir 1000 URLs, criar um novo sitemap
  if ($countLink === 1000) {

    fwrite($handle, '</urlset>' . PHP_EOL);
    fclose($handle);

    $handle = fopen("sitemap-$filename.xml", 'w+');
    fwrite($handle, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL);
    fwrite($handle, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL);


    fwrite($handle, '  <url>' . PHP_EOL);
    fwrite($handle, '    <loc>' . $urlPage . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $blog['blog_name'] . '</loc>' . PHP_EOL);
    fwrite($handle, '    <lastmod>' . $iso8601Date . '</lastmod>' . PHP_EOL);
    fwrite($handle, '    <changefreq>weekly</changefreq>' . PHP_EOL);
    fwrite($handle, '    <priority>0.8</priority>' . PHP_EOL);
    fwrite($handle, '  </url>' . PHP_EOL);

    $countLink = 0;

    $filename++;
  }


endforeach;


fwrite($handle, '</urlset>' . PHP_EOL);
fclose($handle);



$handle = fopen("sitemap.xml", 'w+');
fwrite($handle, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL);
fwrite($handle, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL);

for ($i = 0; $i < $filename; $i++) {
  $i = $i + 1;
  fwrite($handle, '  <url>' . PHP_EOL);
  fwrite($handle, '    <loc>' . $urlPage . "sitemap-$i.xml" . '</loc>' . PHP_EOL);
  fwrite($handle, '    <lastmod>' . $iso8601Date . '</lastmod>' . PHP_EOL);
  fwrite($handle, '    <changefreq>weekly</changefreq>' . PHP_EOL);
  fwrite($handle, '    <priority>0.8</priority>' . PHP_EOL);
  fwrite($handle, '  </url>' . PHP_EOL);
}

fwrite($handle, '</urlset>' . PHP_EOL);
fclose($handle);