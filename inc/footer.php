<div class="clear"></div>
<footer>

	<div class="wrapper-footer">
		<div class="contact-footer">
			<address>
				<span><?= $nomeSite . " - " . $slogan ?></span>
			</address>
			<br>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a href="<?= $url ?>" title="Página inicial">Inicio</a></li>
					<li><a href="<?= $url ?>produtos" title="Produtos">Produtos</a></li>
					<li><a href="<?= $url ?>sobre-nos" title="Sobre nós ">Sobre nós</a></li>
					<li><a rel="nofollow" href="<?= $url ?>mapa-site" title="Sobre a Empresa">Mapa do Site</a></li>
					<li><a href="https://faca-parte.solucoesindustriais.com.br/" target="_blank"><span class="fas-icons"><i class="fas fa-bullhorn"></i></span>Faça Parte</a></li>
				</ul>
			</nav>
		</div>
	</div>
</footer>
<div class="copyright-footer">
	<div class="wrapper-copy">
		Copyright ©
		<?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
		<div class="center-footer"><img class="logoFooter" src="<?= $url ?>imagens/logo-topo.png" alt="Circuito" title="Circuito">
			<p>é um parceiro</p>
			<img src="<?= $url ?>imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
		</div>
		<div class="selos">
			<a rel="nofollow noopener" href="https://validator.w3.org/check?uri=<?= $url . $urlPagina ?>" target="_blank" title="HTML5 W3C"><i class="fa fa-html5"></i> <strong>W3C</strong></a>
			<a rel="nofollow noopener" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $url . $urlPagina ?>" target="_blank" title="CSS W3C"><i class="fa fa-css3"></i> <strong>W3C</strong></a>
		</div>
	</div>
</div>

<?php include "inc/readmore.php"; ?>

<script async src="<?= $url ?>js/vendor/modernizr-2.6.2.min.js"></script>
<script async src="<?= $url ?>js/maskinput.js"></script>

<!-- MENU  MOBILE -->
<script src="<?= $url ?>js/jquery.slicknav.js"></script>
<!-- /MENU  MOBILE -->
<script src="<?= $url ?>js/lcs.js"></script>

<script defer src="<?= $url ?>js/geral.js"></script>

<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js" async></script>
<!-- Shark Orcamento -->
<div id="sharkOrcamento" style="display: none;"></div>
<script>
	var guardar = document.querySelectorAll('.botao-cotar');
	for (var i = 0; i < guardar.length; i++) {
		guardar[i].removeAttribute('href');
		var adicionando = guardar[i].parentNode;
		adicionando.classList.add('nova-api');
	};
</script>

<script src="//code.jivosite.com/widget/PEEPsVuNOq" async></script> 

<!-- BOTAO SCROLL -->

<link rel="stylesheet" href="<?= $url ?>css/normalize.css" type="text/css">
<!-- /BOTAO SCROLL -->
<script src="<?= $url ?>js/click-actions.js"></script>
<script src="<?= $url ?>js/app.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css">

    <!-- Script Launch start -->
    <script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
    <script>
        const aside = document.querySelector('aside');
        const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>';
        aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
    </script>
    <!-- Script Launch end -->
<!-- <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script> -->

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-0B9WEE9KEN"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'G-0B9WEE9KEN');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'G-D23WW3S4NC');
</script>  

        <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>