<li>
	<a href="
		<?=$url?>alarme-de-incendio-categoria" title="Alarme de Incêndio">Alarme de Incêndio
	</a>
	<ul>
		<?php include_once('inc/alarme-de-incendio/mapa-alarme-de-incendio.php');?>
	</ul>
</li>
<li>
	<a href="
		<?=$url?>central-de-alarme-de-incendio-categoria" title="Central de Alarme de Incêndio">Central de Alarme de Incêndio
	</a>
	<ul>
		<?php include_once('inc/central-de-alarme-de-incendio/mapa-central-de-alarme-de-incendio.php');?>
	</ul>
</li>
<li>
	<a href="
		<?=$url?>curso-categoria" title="Curso">Curso
	</a>
	<ul>
		<?php include_once('inc/curso/mapa-curso.php');?>
	</ul>
</li>
<li>
	<a href="
		<?=$url?>curso-de-bombeiro-categoria" title="Curso de Bombeiro">Curso de Bombeiro
	</a>
	<ul>
		<?php include_once('inc/curso-de-bombeiro/mapa-curso-de-bombeiro.php');?>
	</ul>
</li>
<li>
	<a href="
		<?=$url?>curso-de-brigada-de-incendio-categoria" title="Curso de Brigada de Incêndio">Curso de Brigada de Incêndio
	</a>
	<ul>
		
		<?php include_once('inc/curso-de-brigada-de-incendio/mapa-curso-de-brigada-de-incendio.php');?>
		
	</ul>
</li>
<li>
	<a href="
		<?=$url?>extintor-de-incendio-categoria" title="Extintor de Incêndio">Extintor de Incêndio
	</a>
	<ul>
		
		<?php include_once('inc/extintor-de-incendio/mapa-extintor-de-incendio.php');?>
		
	</ul>
</li>
<li>
	<a href="
		<?=$url?>incendio-categoria" title="Incêndio">Incêndio
	</a>
	<ul>
		
		<?php include_once('inc/incendio/mapa-incendio.php');?>
		
	</ul>
</li>
<li>
	<a href="
		<?=$url?>mangueira-categoria" title="Mangueira">Mangueira
	</a>
	<ul>
		
		<?php include_once('inc/mangueira/mapa-mangueira.php');?>
		
	</ul>
</li>
<li>
	<a href="
		<?=$url?>mangueira-de-incendio-categoria" title="Mangueira de Incêndio">Mangueira de Incêndio
	</a>
	<ul>
		
		<?php include_once('inc/mangueira-de-incendio/mapa-mangueira-de-incendio.php');?>
		
	</ul>
</li>
<li>
	<a href="
		<?=$url?>mangueira-hidrante-categoria" title="Mangueira Hidrante">Mangueira Hidrante
	</a>
	<ul>
		
		<?php include_once('inc/mangueira-hidrante/mapa-mangueira-hidrante.php');?>
		
	</ul>
</li>
<li>
	<a href="
		<?=$url?>sistema-de-combate-a-incendio-categoria" title="Sistema de Combate a Incêndio">Sistema de Combate a Incêndio
	</a>
	<ul>
		
		<?php include_once('inc/sistema-de-combate-a-incendio/mapa-sistema-de-combate-a-incendio.php');?>
		
	</ul>
</li>
<li>
	<a href="
		<?=$url?>treinamento-de-primeiros-socorros-categoria" title="Treinamento de Primeiros Socorros">Treinamento de Primeiros Socorros
	</a>
	<ul>
		
		<?php include_once('inc/treinamento-de-primeiros-socorros/mapa-treinamento-de-primeiros-socorros.php');?>
		
	</ul>
</li>