<!DOCTYPE html>
<html class="no-js" lang="pt-br">

<head>
	<meta charset="utf-8">

	<?php include 'inc/geral.php';
	include 'inc/jquery.php' ?>
	<!-- <script>
		<? include("js/jquery-1.9.0.min.js"); ?>
	</script> -->

	<link rel="preload" as="style" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
		onload="this.rel='stylesheet'">



	<link rel="stylesheet" href="<?= $url ?>css/font-awesome.css" type="text/css">
	<link rel="stylesheet" href="<?= $url ?>js/sweetalert/css/sweetalert.css" type="text/css">
	<script src="<?= $url ?>js/sweetalert/js/sweetalert.min.js"></script>

	<script async src="<?= $url ?>js/vendor/modernizr-2.6.2.min.js"></script>
	<link rel="stylesheet" href="<?= $url ?>css/normalize.css" type="text/css">
	<link rel="stylesheet" href="<?= $url ?>css/style.css" type="text/css">

	<?php include 'inc/fancy.php'; ?>

	<title>
		<?= $title . " - " . "TCI" ?>
	</title>
	<base href="<?= $url ?>">
	<meta name="description" content="<?= ucfirst($desc) ?>">
	<meta name="keywords" content="<?= $h1 . " , " . $key ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="geo.position" content="<?= $latitude . " ; " . $longitude ?>">
	<meta name="geo.placename" content="<?= $cidade . " - " . $UF ?>">
	<meta name="geo.region" content="<?= $UF ?>-BR">
	<meta name="ICBM" content="<?= $latitude . " ; " . $longitude ?>">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<meta name="theme-color" content="#B90E1C" />
	<link rel="canonical" href="<?= $url . $urlPagina ?>">
	<?php
	if ($author == '') {
		echo '<meta name="author" content="' . $nomeSite . '">';
	} else {
		echo '<link rel="author" href="' . $author . '">';
	}
	?>
	<link rel="shortcut icon" href="<?= $url ?>imagens/logo-topo.png">
	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?= $title . " - " . $nomeSite ?>">
	<meta property="og:type" content="article">
	<?php
	if (file_exists($url . $pasta . $urlPagina . "-01.jpg")) {
		?>
		<meta property="og:image" content="<?= $url . $pasta . $urlPagina ?>-01.jpg">
		<?php
	}
	?>
	<meta property="og:url" content="<?= $url . $urlPagina ?>">
	<meta property="og:description" content="<?= $desc ?>">
	<meta property="og:site_name" content="<?= $nomeSite ?>">
	<script>
		if ("serviceWorker" in navigator) {
			if (navigator.serviceWorker.controller) {
				console.log("[PWA Builder] active service worker found, no need to register");
			} else {
				// Register the service worker
				navigator.serviceWorker
					.register("pwabuilder-sw.js", {
						scope: "./"
					})
					.then(function (reg) {
						console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
					});
			}
		};
	</script>

	<link rel="manifest" href="/manifest.json">