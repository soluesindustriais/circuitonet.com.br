<?
$nomeSite = 'Circuito Net';
$slogan = 'Cote com as melhores empresas placas de circuito impresso';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
	$url = $http . "://" . $host . "/";
} else {
	$url = $http . "://" . $host . $dir["dirname"] . "/";
}

$ddd = '11';
$fone = '9999-0000';
// $fone2				= '2222-4444';
// $fone3				= '2123-4444';
$emailContato = 'everton.lima@solucoesindustriais.com.br';
$rua = 'Rua Pequetita, 179';
$bairro = 'Vila Olimpia';
$cidade = 'São Paulo';
$UF = 'SP';
$cep = 'CEP: 04552-060';
$latitude = '-22.546052';
$longitude = '-48.635514';
$idCliente = '123456778';
$idAnalytics = 'UA-116954335-1';
$senhaEmailEnvia = '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode = explode("/", $_SERVER['PHP_SELF']);
$urlPagina = end($explode);
$urlPagina = str_replace('.php', '', $urlPagina);
$urlPagina == "index" ? $urlPagina = "" : "";
$urlAnalytics = str_replace("https://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
// $siteKey = '6LcCSEoUAAAAACYcjMbK6D-HFmnwUYcr_EL360fF';
// $secretKey = '6LcCSEoUAAAAAE3pJvIIkCl89EGUfvGm-zjgHBzW';
//Breadcrumbs
$caminho2 = '<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="3" />
</li>
</ol>
</nav>
</div>
</div>
</div>';

$caminho = '
	<div class="breadcrumb" id="breadcrumb">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="' . $url . '" itemprop="item" title="Home">
			<span itemprop="name"> Home ❱  </span>
		  </a>
		  <meta itemprop="position" content="1" >
		</li>   
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">' . $h1 . '</span>
		  <meta itemprop="position" content="2" >
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
';
// breadcumbs

$caminho = '
	<div class="breadcrumb" id="breadcrumb">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="' . $url . '" itemprop="item" title="Home">
			<span itemprop="name"> Home ❱  </span>
		  </a>
		  <meta itemprop="position" content="1" >
		</li>   
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">' . $h1 . '</span>
		  <meta itemprop="position" content="2" >
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
';

$caminhoInfo = '
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="https://www.circuitonet.com.br" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="' . $url . 'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

$caminhoProdutos = '
<div id="breadcrumb" itemscope itemtype="https://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="https://www.circuitonet.com.br" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
		<a href="' . $url . 'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >' . $h1 . '</span></strong>
		</div>
	</div>
</div>
';

// CAIXA PALLET
$caminhocircuito_impresso = '<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'circuito-impresso-categoria" itemprop="item" title="Categoria - Circuito Impresso">
    <span itemprop="name">Categoria - Circuito Impresso »  </span>
  </a>
  <meta itemprop="position" content="3" />
</li> 
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>';

// CAIXA PALLET
$caminhoplaca_pci = '<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'placa-pci-categoria" itemprop="item" title="Categoria - Placa PCI">
    <span itemprop="name">Categoria - Placa PCI »  </span>
  </a>
  <meta itemprop="position" content="3" />
</li> 
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>';

// CAIXA PALLET
$caminhoplaca_de_circuito_impresso_profissional = '<div class="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="' . $url . '" itemprop="item" title="Home">
    <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
    <span itemprop="name">Produtos »  </span>
  </a>
  <meta itemprop="position" content="2" />
</li>   
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="' . $url . 'placa-de-circuito-impresso-profissional-categoria" itemprop="item" title="Categoria - Placa de Circuito Impresso Profissional">
    <span itemprop="name">Categoria - Placa de Circuito Impresso Profissional »  </span>
  </a>
  <meta itemprop="position" content="3" />
</li> 
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">' . $h1 . '</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>';


// CAIXA PALLET
?>