User-agent: *
Disallow: /inc/
Disallow: /imagens/alarme-de-incendio/thumbs/
Disallow: /imagens/central-de-alarme-de-incendio/thumbs/
Disallow: /imagens/curso/thumbs/
Disallow: /imagens/curso-de-bombeiro/thumbs/
Disallow: /imagens/curso-de-brigada-de-incendio/thumbs/
Disallow: /imagens/extintor-de-incendio/thumbs/
Disallow: /imagens/incendio/thumbs/
Disallow: /imagens/mangueira/thumbs/
Disallow: /imagens/mangueira-de-incendio/thumbs/
Disallow: /imagens/mangueira-hidrante/thumbs/
Disallow: /imagens/sistema-de-combate-a-incendio/thumbs/
Disallow: /imagens/treinamento-de-primeiros-socorros/thumbs/

Sitemap: https://www.tudocontraincendio.com.br/sitemap.xml
Sitemap: https://www.tudocontraincendio.com.br/images_sitemap.xml