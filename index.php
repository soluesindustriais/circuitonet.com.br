<?
   $h1         = 'Circuito Net';
   $title      = 'Cotações de Placas de Circuito, Placas PCI e Placas de circuito profissional';
   $desc       = 'Soluçoes para serviços e produtos de eletroeletrônica, entre em contato conosco e faça uma cotação de palcas de circuito. É gratis.';
   $key        = '';
   $var        = 'Início';  
   include('inc/head.php');
   include('inc/fancy.php');
   ?>
   <!--/Máscara dos campos-->
   </head>
   <body>
      <div class="theme-default">
         <div class="wrapper-intro">
            <? include('inc/topo.php'); ?>
                              <div class="fullscreen-bg overlay">
                     <video  muted loop class="fullscreen-bg__video" poster="<?=$url?>imagens/slider/background-intro.jpg">
                        <source src="<?=$url?>imagens/video/background-circuito-eletronico.mp4" type="video/webm">
                        <source src="<?=$url?>imagens/video/background-circuito-eletronico.mp4" type="video/mp4">
                        <source src="<?=$url?>imagens/video/background-circuito-eletronico.mp4" type="video/ogg">
                     </video>
                  </div>
               <section class="wrapper" >

                  <div class="main-content" data-anime="in" >
                     <h1>Placas de Circuito</h1>
                     <h2>Cotações de placas de circuitos</h2>
                     <p>Tenha acesso a uma ampla rede de fornecedores e garanta segurança para o seu produto por um preço que cabe no seu bolso!
                     </p>
                  </div>
                  <span class="btn-space" data-anime="in">
            <button onClick="location.href='<?=$url?>produtos'" class="button__label btn-saiba wow animated fadeIn" ><b>Saiba Mais</b> 
            </button>
            </span>
               </section>
               <span class="scroll-down">
               <!-- </i> -->
            </span>
         </div>
      </div>
      <main>
         <section class="wrapper-main">
            <div class="col-4 incomplete-box" data-anime="left">
               <br class="clear">
               <br class="clear">
               <img class="js-lazy-image" src="<?=$url?>imagens/load.png" data-src="<?=$url?>imagens/slider/pci1.png" alt="Placa-PCI" title="Placas PCI">
            </div>
            <blockquote class="col-4 quadro-2">
               <div>
                  <br>
                  <i class="fa fa-circle fa-5x"></i>
               </div>
               <ul>
                  <li>
                     <h3 style="font-size:20px; line-height: 28px;">Possuindo centenas de anunciantes, o Soluções Industriais é a solução business to business mais completo do ramo.
Para solicitar uma cotação de Produtos, encontre aqui empresas especializadas em:</h>
                  </li>
                  <li>- CIRCUITO IMPRESSO</li>
                  <li>- CIRCUITO IMPRESSO PROFISSIONAL</li>
                  <li>- PLACA PCI</li>
                  </ul>
                  <p>Realize uma cotação de Circuito Impresso, conheça os melhores fornecedores, cote produtos já com centenas de empresas</p>
               <a href="<?=$url?>produtos" class=" btn-saiba" title="veja mais produtos">Veja nossa lista de produtos</a>
               <br class="clear">
            </blockquote>
         </section>
        
         <section style="
    background-color: #00dcff40;">
            <div class="fundo-relacionados txtcenter">
               <h2 class="wow fadeIn">Produtos e Serviços Relacionados</h2>
               <br class="clear">
               <div class="fundo-img" data-anime="up">
                  <img class="js-lazy-image icones-svg" src="<?=$url?>imagens/load.png" data-src=" <?=$url?>imagens/pcb.png" alt="Placa de circuito profissional" title="Placa de circuito impresso profissional">
                  <div class="middle">
                     <a href="<?=$url?>placa-de-circuito-impresso-profissional" title="Placa de circuito impresso profissional">
                        <span class="btn-quadro-menu">Saiba Mais </span>
                     </a>
                  </div>
               </div>
               <div class="fundo-img" data-anime="up">
                  <img class="js-lazy-image icones-svg" src="<?=$url?>imagens/load.png" data-src="<?=$url?>imagens/table-football.png" alt="Placa-pci" title="Placa PCI" >
                  <div class="middle">
                     <a href="<?=$url?>placa-pci-categoria" title="Placa PCI"> 
                  <span class="btn-quadro-menu">Saiba Mais </span>
                  </a>
                  </div>
               </div>
               <div class="fundo-img" data-anime="up">
                  <img class="js-lazy-image icones-svg" src="<?=$url?>imagens/load.png" data-src="<?=$url?>imagens/circuit-board.png" alt="Circuito impresso" title="Circuito impresso">
                  <div class="middle">
                     <a href="<?=$url?>circuito-impresso-categoria" title="Circuito Impresso">
                        <span class="btn-quadro-menu">Saiba Mais </span>
                     </a>
                  </div>
               </div>
            </div>
         </section>
         <section class="wrapper-destaque">
            <div class="wrapper">
               <div class="destaque txtcenter" data-anime="in">
                  <h2>Galeria</h2>
                  <div class="center-block txtcenter">
                     <ul class="gallery">
                        <li><a href="<?=$url?>imagens/portal/circuito-impresso-1.jpg" class="lightbox" title="Circuito Impresso">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/circuito-impresso-1.jpg" alt="Circuito impresso" title="Circuito impresso">
                        </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/circuito-impresso-2.jpg" class="lightbox" title="Circuitos impresso">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/circuito-impresso-2.jpg" alt="Circuitos impresso" title="Circuitos impresso">
                        </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/placa-de-circuito-impresso-profissional-1.jpg" class="lightbox" title="Venda de placas de circuito impresso">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/placa-de-circuito-impresso-profissional-1.jpg" alt="Venda de placas de circuito impresso" title="Venda de placas de circuito impresso">
                           </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/placa-de-circuito-impresso-profissional-2.jpg" class="lightbox" title="Venda de placas de circuito impresso profissional">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/placa-de-circuito-impresso-profissional-2.jpg" alt="Venda de placas de circuito impresso profissional" title="Venda de placas de circuito impresso profissional">
                        </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/placa-de-circuito-impresso-profissional-4.jpg" class="lightbox" title="Placas de Circuito imporesso preço">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/placa-de-circuito-impresso-profissional-4.jpg" alt="Placas de Circuito imporesso preço" title="Placas de Circuito imporesso preço">
                        </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/placa-pci-1.jpg" class="lightbox" title="Placas PCI">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/placa-pci-1.jpg" alt="Placas PCI" title="Placas PCI">
                        </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/placa-pci-2.jpg" class="lightbox" title="Placas Pci Venda">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/placa-pci-2.jpg" alt="Placas Pci Venda" title="Placas Pci Venda">
                           </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/circuito-impresso-4.jpg" class="lightbox" title="Circuito imporesso a venda">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/circuito-impresso-4.jpg" alt="Circuito imporesso a venda" title="Circuito imporesso a venda">
                        </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/placa-de-circuito-impresso-profissional-8.jpg" class="lightbox" title="Placas profissionais de circuito impresso">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/placa-de-circuito-impresso-profissional-8.jpg" alt="Placas profissionais de circuito impresso" title="Placas profissionais de circuito impresso">
                        </a>
                        </li>
                        <li><a href="<?=$url?>imagens/portal/placa-pci-4.jpg" class="lightbox" title="Placas PCI de Circuito impresso">
                           <img class="js-lazy-image" src="<?=$url?>imagens/portal/thumbs/placa-pci-4.jpg" alt="Placas PCI de Circuito impresso" title="Placas PCI de Circuito impresso">
                        </a>
                        </li>

                     </ul>
                  </div>
               </div>
            </div>
            <br class="clear">
            <br class="clear">
         </section>
      </main>
      <? include('inc/footer.php'); ?>
      <script type="text/javascript" src="js/lazy-load.js"></script>
   </body>

   </html>
