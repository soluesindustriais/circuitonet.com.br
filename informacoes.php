<?
$h1       = 'Informações';
$title    = 'Informações';
$desc     = 'Informações, o canal de cotações Tudo Contra Incêndio foi criado com o objetivo de atender o mercado com produtos de incêndio, facilitando a busca do...';
$key      = 'yyyyyyyy, xxxxxxxxx, zzzzzzzzzz';
$var    = 'Informação';
include('inc/head.php');
?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
  <main>
    <div class="content">
      <section>
        <?=$caminho?>
        <h1><?=$h1?></h1>
        <article class="full">
          <section itemscope itemtype="http://schema.org/Service">
            <p>Conheça todas as Informações da <?=$nomeSite.' '.$slogan?>:</p>
            <ul class="thumbnails-main">
              <li>
                <a rel="nofollow" href="<?=$url;?>curso-socorrista" title="Curso de Socorrista"><img src="<?=$url;?>imagens/informacoes/curso-socorrista-01.jpg" alt="Curso de Socorrista" title="Curso de Socorrista"/></a>
                <h2><a href="<?=$url;?>curso-socorrista" title="Curso de Socorrista">Curso de Socorrista</a></h2>
              </li>
              <li>
                <a rel="nofollow" href="<?=$url;?>curso-bombeiro" title="Curso de Bombeiro"><img src="<?=$url;?>imagens/informacoes/curso-bombeiro-01.jpg" alt="Curso de Bombeiro" title="Curso de Bombeiro"/></a>
                <h2><a href="<?=$url;?>curso-bombeiro" title="Curso de Bombeiro">Curso de Bombeiro</a></h2>
              </li>
              <li>
                <a rel="nofollow" href="<?=$url;?>curso-nr10" title="Curso de Nr10"><img src="<?=$url;?>imagens/informacoes/curso-nr10-01.jpg" alt="Curso de Nr10" title="Curso de Nr10"/></a>
                <h2><a href="<?=$url;?>curso-nr10" title="Curso de Nr10">Curso de Nr10</a></h2>
              </li>
              <li>
                <a rel="nofollow" href="<?=$url;?>curso-primeiros-socorros" title="Curso de primeiros socorros"><img src="<?=$url;?>imagens/informacoes/curso-primeiros-socorros-02.jpg" alt="Curso de primeiros socorros" title="Curso de primeiros socorros"/></a>
                <h2><a href="<?=$url;?>curso-primeiros-socorros" title="Curso de primeiros socorros">Curso de primeiros socorros</a></h2>
              </li>
              <li>
                <a rel="nofollow" href="<?=$url;?>curso-bombeiro-civil" title="curso de bombeiro civil"><img src="<?=$url;?>imagens/informacoes/curso-bombeiro-civil-01.jpg" alt="curso de bombeiro civil" title="curso de bombeiro civil"/></a>
                <h2><a href="<?=$url;?>curso-bombeiro-civil" title="curso de bombeiro civil">curso de bombeiro civil</a></h2>
              </li>
            </ul>
          </section>
        </article>
      </section>
    </div>
  </main>
</div>
<? include('inc/footer.php');?>
</body>
</html>