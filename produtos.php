<?
$h1 = 'Produtos';
$title = 'Produtos';
$desc = 'Encontre diversas empresas para soluções em sistemas de incendio, hidrantes, cursos de bombeiro e treinamentos de primeiros socorros.';
$key = 'produtos, hidrante, extintor';
$var = 'Produtos';
include('inc/head.php');
?>
</head>

<body>
    <? include('inc/topo.php'); ?>
    <div class="wrapper">
        <main>
            <div class="content">
                <!-- <div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/breadcrumb">
                    <a rel="home" itemprop="url" href="<?= $url ?>" title="home"><span itemprop="title"><i
                                class="fa fa-home" aria-hidden="true"></i> home</span></a> »
                    <strong><span class="page" itemprop="title">Produtos</span></strong>
                </div> -->
                <div class="contain-bread">
                    <?php echo $caminho ?>
                </div>
                <h1>Produtos</h1>
                <article class="full">
                    <p>Encontre diferentes tipos de soluções antí-incêndio para suas necessidades, solicite agora mesmo
                        um orçamento online com mais de 50 empresas ao mesmo tempo.</p>
                    <ul class="thumbnails-main">

                        <li>
                            <a rel="nofollow" href="<?= $url ?>circuito-impresso-categoria" title="Circuito Impresso"><img
                                    src="<?= $url ?>imagens/portal/thumbs/circuito-impresso_10983_55512_1533158322629_cover.jpg"
                                    alt="circuito-impresso" title="Circuito Impresso" /></a>
                            <h2><a href="<?= $url ?>circuito-impresso-categoria" title="Circuito Impresso">Circuito
                                    Impresso</a></h2>
                        </li>

                        <li>
                            <a rel="nofollow" href="<?= $url ?>placa-pci-categoria" title="Placa PCI"><img
                                    src="<?= $url ?>imagens/placa-pci/placa-pci-2.jpg" alt="Placa-pci"
                                    title="Placa PCI" /></a>
                            <h2><a href="<?= $url ?>placa-pci-categoria" title="Placa PCI">Placa PCI</a></h2>
                        </li>

                        <li>
                            <a rel="nofollow" href="<?= $url ?>placa-de-circuito-impresso-profissional-categoria"
                                title="Placa de Circuito Impresso Profissional Categoria"><img
                                    src="<?= $url ?>imagens/placa-de-circuito-impresso-profissional/placa-de-circuito-impresso-profissional-2.jpg"
                                    alt="curso" title="Placa de Circuito Impresso Profissional Categoria " /></a>
                            <h2><a href="<?= $url ?>placa-de-circuito-impresso-profissional-categoria"
                                    title="Placa de Circuito Impresso Profissional Categoria">Placa de Circuito Impresso
                                    Profissional Categoria</a></h2>
                        </li>

                    </ul>
                </article>
            </div>
        </main>
    </div>
    <? include('inc/footer.php'); ?>
</body>

</html>